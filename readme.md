# Introduction
This exam is designed to test your existing Android development skills and your ability to learn and adapt to new challenges. Please read the following instructions throughly before proceeding to implementation.  
You are expected to be familiar with working in the following areas

-   Implementing network requests using Retrofit library

-   Working with sqlite database using Sugar ORM
    
-   Creating charts using libraries of your choice


# What you will make

You will be implementing a portion of a Financial application. You will consume a web API to fetch a list of Customers and their Saving Accounts. You will implement a page which will display the lists of customers.  When the user selects a customer you will display a new Details activity which will show the selected customer's saving accounts data in a pie chart. The details are described below.

# Tasks

 - Fork the starter project from this repository
 - Create a Customer model class
 - Create a SavingAccount model class
 - Create a Customer List Activity
 - Create a Details Activity 



# CustomerListActivity tasks

- Fetch a list of customers from [the web API route](#get-customers) and store this list

of customers in the Sqlite db

- Display this list of customers from the database in a Recycler view. When an item is clicked on the recycler view start the Details Activity

  
<br/>

# DetailsActivity tasks
- Using the chart library of your choice, display a Pie chart of the saving accounts data for the selected customer. The pie chart should show that name of the saving accounts, how much money is stored in each saving account and what percentage of the total savings is in that particular saving account. Fetch the saving account date from  [here](#get-saving_accountscustomer_idcustomer_id) Here is a reference of what it should look like

![image](http://drive.google.com/uc?export=view&id=1LrzmleCnjcjSmFi4mw5fhCRMw6DbRfx7)


# Implementation Notes
- Write your code using Java
- Make sure you don't run database queries and network requests on the UI Thread. Use an AsyncTask or a new Thread 
- Your code should be clean, organized and readable
- You are free to implement UI however you want but your designs should follow Google's material design

<br/>

# Models
## Customer Model

| Name | Type |
|--|--|
| id | Long |
| firstName | String |
| lastName | String |
| dateCreated | Date |
<br/>

## SavingAccount Model

| Name | Type |
|--|--|
| id | Long |
| name | String |
| balance | Double |
| customerId | Long |
| dateCreated | Date |

<br/>
<br/>

# API Endpoints
 The API's root url is https://my-json-server.typicode.com/Miiiiiike/android_exam_dummy_api/
 
  # *[GET]*  /customers

**This route is for fetching a list of all customers**

Example response:

    [
        {
        “id”:1,
        "first_name": “John”,
        "last_name": “Doe”,
        "date_created" : "Sat Aug 12 00:00:02 GMT+03:00 2019",
        },
        {
        “id”:2,
        "first_name": "Jane",
        "last_name": “Doe”,
        "date_created" : "Sat Aug 14 00:00:03 GMT+03:00 2019",
        },
    ]

<br/>

 # *[GET]* /saving_accounts?customer_id=<customer_id>
**This route is for fetching all saving accounts of a customer, you will pass the *<customer_id>* as a query parameter as shown above**

Example <br/> **Query:**

    https://my-json-server.typicode.com/Miiiiiike/android_exam_dummy_api/saving_accounts?customer_id=1


**Response:**

    [
        {
            "id": 1,
            "name": "Dashen",
            "current_balance": 1254.0,
            "customer_id": 1,
            "date_created": "Sat Aug 21 00:00:04 GMT+03:00 2019"
        },
        {
            "id": 2,
            "name": "Dashen",
            "current_balance": 3759.0,
            "customer_id": 1,
            "date_created": "Sat Aug 22 00:00:05 GMT+03:00 2019
        }
    ]

<br/>
<br/>

# Bonus task
- In the DetailsActivity display a table using a library of your choice to show a all the saving accounts of a customer like the following *(You can display this table under the chart or you can create a separate tab in the activity so there can be a tab for the chart and the table)*


|Name| Balance|
|-|-|
|CBE| 12,345.00|
|Dashen Bank| 33,832.23|
|Bunna Bank| 17,086.06|

<br/>


# Good luck!